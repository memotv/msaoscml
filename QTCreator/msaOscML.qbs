import qbs

CppApplication {
    consoleApplication: true
    files: [
        "../src/*",
        "../src/libs/ofxOsc/src/*",
        "../src/libs/oscpack/*",
        "../src/libs/oscpack/osc/*",
        "../src/libs/oscpack/ip/*",
        "../src/libs/oscpack/ip/posix/*",
//        "../src/libs/Fann/src/*",
//        "../src/libs/Fann/src/include/*",
        "../src/libs/GRT/*",
        "../src/libs/GRT/*/*",
        "../src/libs/GRT/*/*/*",
        "../src/libs/GRT/*/*/*/*",
    ]

    cpp.includePaths: [
        "../src/",
        "../src/libs/",
        "../src/libs/ofxOsc/src/",
        "../src/libs/oscpack/",
        "../src/libs/oscpack/osc/",
        "../src/libs/oscpack/ip/",
        "../src/libs/oscpack/ip/posix/",
//        "../src/Fann/src/",
//        "../src/Fann/src/include/",
        "../src/libs/GRT/",
        "../src/libs/GRT/*/",
        "../src/libs/GRT/*/*/",
        "../src/libs/GRT/*/*/*/",
    ]

    cpp.dynamicLibraries: [ 'pthread' ]

    cpp.cxxFlags: [ '-std=c++11', '-std=gnu++11', '-pthread' ]

    Group {     // Properties for the produced executable
        fileTagsFilter: product.type
        qbs.install: true
    }
}
