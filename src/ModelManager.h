//                                     __
//    ____ ___  ___  ____ ___  ____   / /__   __
//   / __ `__ \/ _ \/ __ `__ \/ __ \ / __/ | / /
//  / / / / / /  __/ / / / / / /_/ // /_ | |/ /
// /_/ /_/ /_/\___/_/ /_/ /_/\____(_)__/ |___/
//
//
// Created by Memo Akten, www.memo.tv
//

#pragma once




#include "Model.h"
#include "DataSender.h"
#include "MLImplFann.h"
#include "MLImplGrt.h"

#include <iostream>

#include <list>
#include <cstring>
#include <assert.h>
#include <cstdarg>
#include <memory>
#include <iostream>
#include <map>

namespace msa {
    namespace ml {
        
        class DataSender;
        
        
        // Vector type is templated (e.g. could be std::vector<float> or std::vector<double> or any other class that has same API
        template <typename DataVector,  typename T>
        class ModelManager {
        public:
            
            //---------------------------------------------------------------
            // OSC Interface
            // if model_name is blank, loop and do for all current models (except for init_model obviously)
            
			// set verbose level (dumping to console)
			void verbose(std::string model_name, bool b);

            // create regression model with name and dimensions
            void init_model(std::string model_name, int input_dims, int output_dims);
            
            // delete model(s)
            void delete_model(std::string model_name);
            
            // set output vector for a model (only used for regression)
            void set_output_vector(std::string model_name, const DataVector& output_vector);
            
            // add data to model(s) training set
            void add_training_data(std::string model_name, const DataVector& input_vector);
            
            // train data on model(s)
            void train(std::string model_name);
            
            // predict data for model(s)
            void predict(std::string model_name, const DataVector& input_vector, const DataSender& data_sender);
            
            // save data for model(s)
            void save_training_data_to_file(std::string model_name, std::string path);
            void save_training_data_to_folder(std::string model_name, std::string folder);
            
            // load data for model(s)
            void load_training_data_from_file(std::string model_name, std::string path);
            void load_training_data_from_folder(std::string model_name, std::string folder);
            
            // clear data for model(s)
            void clear_training_data(std::string model_name);
            
            // dump model(s) info to console
            void get_summary_info(std::string model_name) const;
            void get_detailed_info(std::string model_name) const;

            
            //---------------------------------------------------------------
            // Util functions

			// get model, returns null if doesn't exist
             typename Model<DataVector, T>::Ptr get(std::string model_name);
			
			// check if model exists
			bool exists(std::string model_name) const { return _models.count(model_name) > 0; }
            
        private:
            std::map <std::string, typename Model<DataVector, T>::Ptr> _models;
			bool _verbose = false;
        };
        

        

        template <typename DataVector, typename T>
        void ModelManager<DataVector, T>::verbose(std::string model_name, bool b) {
            std::cout << "ModelManager::verbose [" << model_name << "] " << b;
            _verbose = b;
            if (model_name.empty()) {
                std::cout << " | model name empty, applying to " << _models.size() << " models" << std::endl;
                for (auto& kv : _models) kv.second->verbose = b;
            }
            else {
                if (!exists(model_name)) {
                    std::cout << " | model doesn't exist" << std::endl;
                }
                else {
                    _models[model_name]->verbose = b;
                }
            }
        }
        
        
        template <typename DataVector, typename T>
        void ModelManager<DataVector, T>::init_model(std::string model_name, int input_dims, int output_dims) {
            std::cout << "ModelManager::init_model";
            if(model_name.empty()) {
                std::cout << " | ERROR: needs model name" << std::endl;
            } else {
                if(exists(model_name)) {
                    std::cout << " | WARNING: model already exists, re-initializing";
                }
                std::cout << " | ";
//                auto m = std::make_shared< Model<T> >(model_name, input_dims, output_dims, std::make_shared< MLImplFann<T> >() ); // TODO: read implementation from OSC
                auto m = std::make_shared< Model<DataVector, T> >(model_name, input_dims, output_dims, std::make_shared< MLImplFann<DataVector, T> >() ); // TODO: read implementation from OSC
                m->verbose = _verbose;
                _models[model_name] = m;
            }
        }
        
        
        
        template <typename DataVector, typename T>
        void ModelManager<DataVector, T>::delete_model(std::string model_name) {
            std::cout << "ModelManager::delete_model [" << model_name << "]";
            if(model_name.empty()) {
                std::cout << " | model name empty, applying to " << _models.size() << " models" << std::endl;
                _models.clear();
            } else {
                if(!exists(model_name)) {
                    std::cout << " | model doesn't exist" << std::endl;
                } else {
                    _models.erase(model_name);
                    std::cout << std::endl;
                }
            }
        }
        
        
        
        template <typename DataVector, typename T>
        void ModelManager<DataVector, T>::set_output_vector(std::string model_name, const DataVector& output_vector) {
            if(model_name.empty()) {
                std::cout << "ModelManager::set_output_vector | model name empty, applying to " << _models.size() << " models" << std::endl;
                for (auto& kv : _models) kv.second->set_output_vector(output_vector);
            } else {
                if(!exists(model_name)) {
                    std::cout << "ModelManager::set_output_vector [" << model_name << "] | model doesn't exist" << std::endl;
                } else {
                    _models[model_name]->set_output_vector(output_vector);
                }
            }
        }
        
        
        template <typename DataVector, typename T>
        void ModelManager<DataVector, T>::add_training_data(std::string model_name, const DataVector& input_vector) {
            if(model_name.empty()) {
                std::cout << "ModelManager::add_training_data | model name empty, applying to " << _models.size() << " models" << std::endl;
                for (auto& kv : _models) kv.second->add_training_data(input_vector);
            } else {
                if(!exists(model_name)) {
                    std::cout << "ModelManager::add_training_data [" << model_name << "] | model doesn't exist" << std::endl;
                } else {
                    _models[model_name]->add_training_data(input_vector);
                }
            }
        }
        
        
        template <typename DataVector, typename T>
        void ModelManager<DataVector, T>::train(std::string model_name) {
            if(model_name.empty()) {
                std::cout << "ModelManager::train | model name empty, applying to " << _models.size() << " models" << std::endl;
                for (auto& kv : _models) kv.second->train();
            } else {
                if(!exists(model_name)) {
                    std::cout << "ModelManager::train [" << model_name << "] | model doesn't exist" << std::endl;
                } else {
                    _models[model_name]->train();
                }
            }
        }
        
        
        template <typename DataVector, typename T>
        void ModelManager<DataVector, T>::predict(std::string model_name, const DataVector& input_vector, const DataSender& data_sender) {
            if(model_name.empty()) {
                if(_verbose) std::cout << "ModelManager::predict | model name empty, applying to " << _models.size() << " models" << std::endl;
                for (auto& kv : _models) predict(kv.first, input_vector, data_sender);
            } else {
                if(!exists(model_name)) {
                    if (_verbose) std::cout << "ModelManager::predict [" << model_name << "] ... WARNING: model doesn't exist" << std::endl;
                } else {
                    DataVector output_vector;
                    _models[model_name]->predict(input_vector, output_vector);
                    data_sender.send_prediction(model_name, input_vector, output_vector);
                }
            }
        }
        
        
        template <typename DataVector, typename T>
        void ModelManager<DataVector, T>::save_training_data_to_file(std::string model_name, std::string path) {
            if(model_name.empty()) {
                std::cout << "ModelManager::save_training_data_to_file | model name empty, applying to " << _models.size() << " models" << std::endl;
                for (auto& kv : _models) kv.second->save_training_data_to_file(path);
            } else {
                if(!exists(model_name)) {
                    std::cout << "ModelManager::save_training_data_to_file [" << model_name << "] | model doesn't exist" << std::endl;
                } else {
                    _models[model_name]->save_training_data_to_file(path);
                }
            }
        }
        
        
        template <typename DataVector, typename T>
        void ModelManager<DataVector, T>::save_training_data_to_folder(std::string model_name, std::string folder) {
            if(model_name.empty()) {
                std::cout << "ModelManager::save_training_data_to_folder | model name empty, applying to " << _models.size() << " models" << std::endl;
                for (auto& kv : _models) kv.second->save_training_data_to_folder(folder);
            } else {
                if(!exists(model_name)) {
                    std::cout << "ModelManager::save_training_data_to_folder [" << model_name << "] | model doesn't exist" << std::endl;
                } else {
                    _models[model_name]->save_training_data_to_folder(folder);
                }
            }
        }
        
        
        
        template <typename DataVector, typename T>
        void ModelManager<DataVector, T>::load_training_data_from_file(std::string model_name, std::string path) {
            if(model_name.empty()) {
                std::cout << "ModelManager::load_training_data_from_file | model name empty, applying to " << _models.size() << " models" << std::endl;
                for (auto& kv : _models) kv.second->load_training_data_from_file(path);
            } else {
                if(!exists(model_name)) {
                    std::cout << "ModelManager::load_training_data_from_file [" << model_name << "] | model doesn't exist" << std::endl;
                } else {
                    _models[model_name]->load_training_data_from_file(path);
                }
            }
        }
        
        
        template <typename DataVector, typename T>
        void ModelManager<DataVector, T>::load_training_data_from_folder(std::string model_name, std::string folder) {
            if(model_name.empty()) {
                std::cout << "ModelManager::load_training_data_from_folder | model name empty, applying to " << _models.size() << " models" << std::endl;
                for (auto& kv : _models) kv.second->load_training_data_from_folder(folder);
            } else {
                if(!exists(model_name)) {
                    std::cout << "ModelManager::load_training_data_from_folder [" << model_name << "] | model doesn't exist" << std::endl;
                } else {
                    _models[model_name]->load_training_data_from_folder(folder);
                }
            }
        }
        
        
        
        template <typename DataVector, typename T>
        void ModelManager<DataVector, T>::clear_training_data(std::string model_name) {
            if(model_name.empty()) {
                std::cout << "ModelManager::clear_training_data | model name empty, applying to " << _models.size() << " models" << std::endl;
                for (auto& kv : _models) kv.second->clear_training_data();
            } else {
                if(!exists(model_name)) {
                    std::cout << "ModelManager::clear_training_data [" << model_name << "] | model doesn't exist" << std::endl;
                } else {
                    _models[model_name]->clear_training_data();
                }
            }
        }
        
        
        
        template <typename DataVector, typename T>
        void ModelManager<DataVector, T>::get_summary_info(std::string model_name) const {
            if(model_name.empty()) {
                std::cout << "ModelManager::get_summary_info | model name empty, applying to " << _models.size() << " models" << std::endl;
                for (auto& kv : _models) kv.second->get_summary_info();
            } else {
                if(!exists(model_name)) {
                    std::cout << "ModelManager::get_summary_info [" << model_name << "] | model doesn't exist" << std::endl;
                } else {
                    _models.find(model_name)->second->get_summary_info();
                }
            }
        }
        
        
        template <typename DataVector, typename T>
        void ModelManager<DataVector, T>::get_detailed_info(std::string model_name) const {
            if(model_name.empty()) {
                std::cout << "ModelManager::get_detailed_info | model name empty, applying to " << _models.size() << " models" << std::endl;
                for (auto& kv : _models) kv.second->get_detailed_info();
            } else {
                if(!exists(model_name)) {
                    std::cout << "ModelManager::get_detailed_info [" << model_name << "] | model doesn't exist" << std::endl;
                } else {
                    _models.find(model_name)->second->get_detailed_info();
                }
            }
        }
        
        
        template <typename DataVector, typename T>
        typename Model<DataVector, T>::Ptr ModelManager<DataVector, T>::get(std::string model_name) {
        if(!exists(model_name)) {
            std::cout << "ModelManager::get [" << model_name << "] | model doesn't exist" << std::endl;
            return  Model<DataVector, T>::Ptr(0);
            } else {
                return _models[model_name];
            }
        }
        
        
    }
}
