//                                     __
//    ____ ___  ___  ____ ___  ____   / /__   __
//   / __ `__ \/ _ \/ __ `__ \/ __ \ / __/ | / /
//  / / / / / /  __/ / / / / / /_/ // /_ | |/ /
// /_/ /_/ /_/\___/_/ /_/ /_/\____(_)__/ |___/
//
//
// Created by Memo Akten, www.memo.tv
//
//
//  main.cpp
//  msaGRT
//
//  Created by Memo Akten on 08/04/2015.
//  Copyright (c) 2015 MSAV. All rights reserved.
//

/*
 Usage similar to http://www.nickgillian.com/wiki/pmwiki.php/GRT/GUI
 but multimodel (multiple training spaces going on. prefix commands with name)
 
 Todo:
 FOR FANN:
 - save, load data
 - save, load training
 - OSC interface to:
    - set activation function
    - steepness of activation function. fann_set_activation_steepness_hidden and fann_set_activation_steepness_output
    - set network properties (number of hidden layers, hidden neurons)
    - set training algorithm fann_set_training_algorithm((FANN_TRAIN_RPROP)
 - classification
 
 
 -----
 SETTINGS
    - OSC inport, targetip, targetport
    - sleep time

 
 OSC SENDER
 
 SAVE/LOAD
 - Not just data, but classifier and classifier options for each Model
 - ModelManager configuration (number, name and types of models)
 
 
 EDITING DATA (remove etc)
 
 CLASSIFIERS vs REGRESSION
 

 PIPELINE
 pipeline.addPreProcessingModule( );
 pipeline.addFeatureExtractionModule( );
 pipeline.setClassifier( ANBC() );
 pipeline.addPostProcessingModule( );
 
 
 NULL REJECTION
 myClassifier.enableNullRejection( true );
 
 TESTING
 // test data
 bool testSuccess = pipeline.test( testData );
 double accuracy = pipeline.getTestAccuracy();
 double fMeasure = pipeline.getTestFMeasure();
 double precision = pipeline.getTestPrecision();
 double recall = pipeline.getTestRecall();
 ---
 k-fold cross validation
 bool trainSuccess = pipeline.train( trainingData, 10 );
 double accuracy = pipeline.getCrossValidationAccuracy();
 
 ClassificationData testData = trainingData.partition(80);

 
 */


#include <iostream>
#include <chrono>
#include <thread>

//#include <unistd.h>


#include "ModelManager.h"
#include "DataReceiver.h"
#include "DataSender.h"

using namespace std;
//using namespace msa::trainer;

int main(int argc, const char * argv[]) {
    msa::ml::vector_utils::test<std::vector<double>, double>();
    

	// TODO: make these customizable (commandline parameter? read from text?)
    int sleepmillis = 1;    // 1000Hz. how many milliseconds to sleep
    int inport = 8000;
    string targetip = "127.0.0.1";
    int targetport = 9000;
	bool showTimes = false;
    
    msa::ml::DataReceiver dataReceiver;
    msa::ml::DataSender dataSender;
//    msa::ml::ModelManager<GRT::VectorFloat, GRT::Float> modelManager;
    msa::ml::ModelManager<vector<fann_type>, fann_type> modelManager; 
    
    std::cout << "msaOscML v0.3 by Memo Akten - www.memo.tv" << endl;
	std::cout << "using GRT by Nick Gillian - www.nickgillian.com/software/grt" << endl;
	std::cout << "and FANN by Steffen Nissen - www.leenissen.dk/fann" << endl;
	std::cout << endl;
    std::cout << "Listening on port: " << inport << endl;
    std::cout << "Sending to: " << targetip << ":" << targetport << endl;

    dataReceiver.setup(inport);
    dataSender.setup(targetip, targetport);
    
    // init timers
	std::chrono::time_point<std::chrono::system_clock> start_time, end_time;
	std::chrono::duration<double> elapsed;
    
    
    // loop forever, and ever, and ever
    while(true) {
        start_time = std::chrono::system_clock::now();
        
        // check for incoming messages, if there are any, update models and send data back
		bool b = dataReceiver.check_incoming(modelManager, dataSender);
        
        // dump time info to console
		if (b) {
			end_time = std::chrono::system_clock::now();
			elapsed = end_time - start_time;
			if (showTimes) std::cout << "                                              elapsed time: " << elapsed.count() << "s" << endl;
		}

        //usleep(sleepmillis * 1000);
		std::this_thread::sleep_for(std::chrono::milliseconds(sleepmillis));
    }
    return 0;
}
