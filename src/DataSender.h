//                                     __
//    ____ ___  ___  ____ ___  ____   / /__   __
//   / __ `__ \/ _ \/ __ `__ \/ __ \ / __/ | / /
//  / / / / / /  __/ / / / / / /_/ // /_ | |/ /
// /_/ /_/ /_/\___/_/ /_/ /_/\____(_)__/ |___/
//
//
// Created by Memo Akten, www.memo.tv
//
//
// Stores a single training data point as an input+output pair
//
//


#pragma once

#include "ofxOscSender.h"
#include <string>
#include <map>

namespace msa {
    namespace ml {
        
        class DataSender {
        public:
            void setup(std::string targetip, int targetport) {
                _oscSender.setup(targetip, targetport);
            }

            template <typename DataVector>
            void send_prediction(const std::string modelName, const DataVector& input_vector, const DataVector& output_vector) const;
        private:
            mutable ofxOscSender _oscSender;
        };


        
        
        template <typename DataVector>
        void DataSender::send_prediction(const std::string modelName, const DataVector& input_vector, const DataVector& output_vector) const {
            string addr = "/Prediction/" + modelName;
            ofxOscBundle b;
            
            // add input vector (TODO: make optional)
            {
                ofxOscMessage m;
                m.setAddress(addr + "/Input");
                for(const auto& f : input_vector) m.addFloatArg(f);
                b.addMessage(m);
            }
            
            // add output vector
            {
                ofxOscMessage m;
                m.setAddress(addr + "/Output");
                for(const auto& f : output_vector) m.addFloatArg(f);
                b.addMessage(m);
            }
            _oscSender.sendBundle(b);
        }
    }
}
