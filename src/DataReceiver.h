//                                     __
//    ____ ___  ___  ____ ___  ____   / /__   __
//   / __ `__ \/ _ \/ __ `__ \/ __ \ / __/ | / /
//  / / / / / /  __/ / / / / / /_/ // /_ | |/ /
// /_/ /_/ /_/\___/_/ /_/ /_/\____(_)__/ |___/
//
//
// Created by Memo Akten, www.memo.tv
//
// Stores a single training data point as an input+output pair
//
//

/*
 Similar to http://www.nickgillian.com/wiki/pmwiki.php/GRT/GUI but multimodel
 so most commands are same or similar, but with a model name prefixed
 also some new commands
 
 */



#pragma once

#include "ofxOscReceiver.h"

#include <iostream>
#include <sstream>
#include <algorithm>

namespace msa {
    namespace ml {
		        
        template <typename DataVector, typename T> class ModelManager;
		class DataSender;

        class DataReceiver {
        public:
            void setup(int listen_port);
            
            template <typename DataVector, typename T>
            bool check_incoming(ModelManager<DataVector, T>& models, const DataSender& dataSender);
            
        private:
            ofxOscReceiver _receiver;
        };
    }
}

        
#include "DataSender.h"
#include "ModelManager.h"

        
namespace msa {
    namespace ml {
        
        inline void DataReceiver::setup(int listen_port) {
            _receiver.setup(listen_port);
        }
        
        inline string to_string(const ofxOscMessage &m) {
            ostringstream msg_string;
            
            msg_string << "From " << m.getRemoteIp() << ":" << m.getRemotePort() << " | ";
            msg_string << m.getAddress() << " ";
            //        msg_string << " | ";
            for(int i = 0; i < m.getNumArgs(); i++){
                // get the argument type
                msg_string << m.getArgTypeName(i);
                msg_string << ":";
                // display the argument - make sure we get the right type
                if(m.getArgType(i) == OFXOSC_TYPE_INT32){
                    msg_string << m.getArgAsInt32(i)  << " ";
                }
                else if(m.getArgType(i) == OFXOSC_TYPE_FLOAT){
                    msg_string << m.getArgAsFloat(i) << " ";
                }
                else if(m.getArgType(i) == OFXOSC_TYPE_STRING){
                    msg_string << m.getArgAsString(i) << " ";
                }
                else{
                    msg_string << "unknown ";
                }
            }
            return msg_string.str();
        }
        
        
        
        inline vector <string> ofSplitString(const string& source, const string& delimiter, bool ignoreEmpty = true) {
            vector<string> result;
            if (delimiter.empty()) {
                result.push_back(source);
                return result;
            }
            string::const_iterator substart = source.begin(), subend;
            while (true) {
                subend = search(substart, source.end(), delimiter.begin(), delimiter.end());
                string sub(substart, subend);
                if (!ignoreEmpty || !sub.empty()) {
                    result.push_back(sub);
                }
                if (subend == source.end()) {
                    break;
                }
                substart = subend + delimiter.size();
            }
            return result;
        }
        
        
        inline bool check_arg_syntax(const ofxOscMessage &m, string argumentTypes, string message) {
            bool error = false;
            bool variableSize = argumentTypes.back() == '*';
            if(variableSize || m.getNumArgs() == argumentTypes.size()) {
                for(int i=0; i<m.getNumArgs(); i++) {
                    char c = argumentTypes[i];
                    ofxOscArgType realArgument = m.getArgType(i);
                    switch (c) {
                        case 's': if(realArgument != OFXOSC_TYPE_STRING) error = true; break;
                        case 'i': if(realArgument != OFXOSC_TYPE_INT32) error = true; break;
                        case 'f': if(realArgument != OFXOSC_TYPE_FLOAT) error = true; break;
                        default:
                            break;
                    }
                }
            } else {
                error = true;
            }
            if(error) cout << "ERROR correct syntax for " <<m.getAddress() << " is [" << message << "]" << endl;
            return !error;
        }
        
        
        
        template <typename DataVector, typename T>
        bool DataReceiver::check_incoming(ModelManager<DataVector, T> &modelManager, const DataSender &dataSender) {
            bool ret = false;
            while(_receiver.hasWaitingMessages()) {
                ret = true;
                ofxOscMessage m;
                _receiver.getNextMessage(&m);
                
                // get command and model name from path by splitting it into tokens separated by "/"
                vector<string> pathTokens = ofSplitString(m.getAddress(), "/");
                if (pathTokens.empty()) continue;
                
                string command = pathTokens[0];
                string modelName = pathTokens.size() == 2 ? pathTokens[1] : "";
                //string command, modelName;
                //if (pathTokens.size() == 2) {
                //	modelName = pathTokens[0];
                //	command = pathTokens[1];
                //} else {
                //	modelName = "";
                //	command = pathTokens[0];
                //}
                
                
                int numArgs = m.getNumArgs();
                
                if (command == "Verbose") {
                    if (check_arg_syntax(m, "i", "b_Verbose")) {
                        bool b_Verbose = (bool)m.getArgAsInt32(0);
                        modelManager.verbose(modelName, b_Verbose);
                    }
                } else if(command == "GetSummaryInfo") {
                    modelManager.get_summary_info(modelName);
                    
                } else if(command == "GetDetailedInfo") {
                    modelManager.get_detailed_info(modelName);
                    
                } else if(command == "InitModel") {
                    if(check_arg_syntax(m, "ii", "i_inputDims i_outputDims")) {
                        int i_inputDims = m.getArgAsInt32(0);
                        int i_outputDims = m.getArgAsInt32(1);
                        modelManager.init_model(modelName, i_inputDims, i_outputDims);
                    }
                    
                } else if(command == "DeleteModel") {
                    modelManager.delete_model(modelName);
                    
                } else if(command == "SetOutputVector") {
                    if(check_arg_syntax(m, "f*", "list of floats")) {
                        DataVector outputVector(numArgs);
                        for(unsigned int i=0; i<outputVector.size(); i++) outputVector[i] = (T)m.getArgAsFloat(i);
                        modelManager.set_output_vector(modelName, outputVector);
                    }
                    
                } else if(command == "AddTrainingData") {
                    if(check_arg_syntax(m, "f*", "list of floats")) {
                        DataVector inputVector(numArgs);
                        for (unsigned int i = 0; i<inputVector.size(); i++) inputVector[i] = (T)m.getArgAsFloat(i);
                        modelManager.add_training_data(modelName, inputVector);
                    }
                    
                } else if(command == "Train") {
                    modelManager.train(modelName);
                    
                } else if(command == "Predict") {
                    if(check_arg_syntax(m, "f*", "list of floats")) {
                        DataVector inputVector(numArgs);
                        for (unsigned int i = 0; i<inputVector.size(); i++) inputVector[i] = (T)m.getArgAsFloat(i);
                        modelManager.predict(modelName, inputVector, dataSender);
                    }
                    
                } else if(command == "SaveTrainingDataToFile") {
                    if(check_arg_syntax(m, "s", "s_filename")) {
                        string s_filename = m.getArgAsString(0);
                        modelManager.save_training_data_to_file(modelName, s_filename);
                    }
                    
                    
                } else if(command == "SaveTrainingDataToFolder") {
                    if(check_arg_syntax(m, "s", "s_foldername")) {
                        string s_foldername = m.getArgAsString(0);
                        modelManager.save_training_data_to_folder(modelName, s_foldername);
                    }
                    
                    
                } else if(command == "LoadTrainingDataFromFile") {
                    if(check_arg_syntax(m, "s", "s_filename")) {
                        string s_filename = m.getArgAsString(0);
                        modelManager.load_training_data_from_file(modelName, s_filename);
                    }
                    
                } else if(command == "LoadTrainingDataFromFolder") {
                    if(check_arg_syntax(m, "s", "s_foldername")) {
                        string s_foldername = m.getArgAsString(0);
                        modelManager.load_training_data_from_folder(modelName, s_foldername);
                    }
                    
                } else if(command == "ClearTrainingData") {
                    modelManager.clear_training_data(modelName);
                    
                } else if(command == "SetNumHiddenLayers") {
                    if(check_arg_syntax(m, "s", "s_foldername")) {
                        string s_foldername = m.getArgAsString(0);
                        modelManager.load_training_data_from_folder(modelName, s_foldername);
                    }
                    
                    
                } else {
                    cout << "Unknown message " << to_string(m);
                    cout << " | command: "  << command;
                    cout << " | modelName: "  << modelName;
                    cout << endl;
                    
                }
                
            }
            return ret;
        }
        

    }
}
